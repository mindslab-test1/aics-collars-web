import React, { Fragment } from "react";

// Components
import { StyledLink } from "../../components/utils/Utils";

const Main = () => {
  return (
    <Fragment>
      <h2>Main Home</h2>
      <StyledLink to="/dashboard">DASHBOARD</StyledLink>
    </Fragment>
  );
};

export default Main;
