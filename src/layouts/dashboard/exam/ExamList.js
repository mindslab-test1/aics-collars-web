import React, { Fragment, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

import { loadDataList } from "../../../actions/exam";

const ExamList = () => {
  const history = useHistory();
  const [current, setCurrent] = useState(0);
  const [data, setData] = useState([]);

  useEffect(() => {
    setCurrent(data.length);
  }, [data, setCurrent]);

  const handleLoadDataList = async (e) => {
    e.preventDefault();
    const newDataList = await loadDataList({ current });
    setData((prev) => [...prev, ...newDataList]);
  };
  const handleUpdate = (id) => (e) => {
    e.preventDefault();
    history.push(`/dashboard/exam/ExamUpdate/${id}`);
  };

  return (
    <Fragment>
      <button onClick={handleLoadDataList}>데이터 가져오기</button>
      {data.map((value, index) => {
        return (
          <div key={index} onClick={handleUpdate(value.id)}>
            <h3>TYPE: {value.type}</h3>
            <p>NAME: {value.name}</p>
          </div>
        );
      })}
    </Fragment>
  );
};

ExamList.propTypes = { loadDataList: PropTypes.func };

export default ExamList;
