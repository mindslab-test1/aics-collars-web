import React, { Fragment, useState } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { changeType, uploadData } from "../../../actions/exam";
// Components
import {
  Level,
  Session,
  EXAM_TYPE_LEVEL,
  EXAM_TYPE_SESSION,
} from "../../../components/exam/Exam";

const ExamAdd = ({ changeType, uploadData, types, currentType, data }) => {
  const [loading, setLoading] = useState(false);
  const [name, setName] = useState("");
  const [duration, setDuration] = useState(0);

  const handleTypes = (e) => changeType({ type: e.target.value });
  const handleName = (e) => setName(e.target.value);
  const handleDuration = (e) => setDuration(e.target.value);
  const handleSubmit = async () => {
    setLoading(true);
    let isSuccess = await uploadData({
      name,
      type: currentType,
      data,
      duration,
    });
    setLoading(false);
  };

  const renderDataForm = () => {
    switch (currentType) {
      case EXAM_TYPE_LEVEL:
        return <Level />;
      case EXAM_TYPE_SESSION:
        return <Session />;
      default:
        break;
    }
  };

  return (
    <Fragment>
      <select onChange={handleTypes} value={currentType}>
        {types.map((value, index) => {
          value = value.toUpperCase();
          return (
            <option key={index} value={value}>
              {value}
            </option>
          );
        })}
      </select>
      <h3>시간</h3>
      <input type="number" onChange={handleDuration} value={duration} />
      <span>초</span>
      <br />
      <h1>{currentType}</h1>
      <h2>이름</h2>
      <input onChange={handleName} value={name} />
      {renderDataForm()}
      {loading ? <p>로딩중</p> : <button onClick={handleSubmit}>보내기</button>}
    </Fragment>
  );
};

ExamAdd.propTypes = {
  types: PropTypes.array,
  currentType: PropTypes.string,
  data: PropTypes.array,
  loadType: PropTypes.func,
  changeType: PropTypes.func,
  uploadData: PropTypes.func,
};

const mapStateToProps = (state) => ({
  types: state.exam.types,
  currentType: state.exam.currentType,
  data: state.exam.data,
});

export default connect(mapStateToProps, {
  changeType,
  uploadData,
})(ExamAdd);
