import React, { Fragment, useEffect } from "react";
import { useHistory, useParams } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { loadData } from "../../../actions/exam";
// Components
import {
  Level,
  Session,
  EXAM_TYPE_LEVEL,
  EXAM_TYPE_SESSION,
} from "../../../components/exam/Exam";

const ExamUpdate = ({ data, currentType, loadData }) => {
  const history = useHistory();
  const { id } = useParams();
  if (id === undefined || id === ":id") history.goBack();

  useEffect(() => {
    loadData({ id });
  }, [id, loadData]);

  const renderDataForm = () => {
    switch (currentType) {
      case EXAM_TYPE_SESSION:
        return <Session />;
      case EXAM_TYPE_LEVEL:
        return <Level />;
      default:
        break;
    }
  };

  return <Fragment>{renderDataForm()}</Fragment>;
};

ExamUpdate.propTypes = {
  currentType: PropTypes.string,
  data: PropTypes.array,
  loadData: PropTypes.func,
};

const mapStateToProps = (state) => ({
  currentType: state.exam.currentType,
  data: state.exam.data,
});

export default connect(mapStateToProps, { loadData })(ExamUpdate);
