import React, { Fragment, useState } from "react";

// Action
import { upload } from "../../../actions/survey";

const SurveyAdd = () => {
  const [question, setQuestion] = useState("");
  const [answers, setAnswers] = useState([]);

  const handlequestion = (e) => setQuestion(e.target.value);
  const handleAnswer = (index) => (e) => {
    const { value } = e.target;
    const newAnswer = [...answers];
    newAnswer[index].answer = value;
    setAnswers(newAnswer);
    console.log(answers);
  };
  const handleResponse = (index) => (e) => {
    const { value } = e.target;
    const newAnswer = [...answers];
    newAnswer[index].response = value;
    setAnswers(newAnswer);
    console.log(answers);
  };
  const handleUpload = (e) => {
    const isSucceed = upload({ question, answers });
    if (isSucceed) {
      setQuestion("");
      setAnswers([]);
    }
  };
  const addAnswer = (e) => {
    e.preventDefault();
    const answerForm = { answer: "", response: "" };
    setAnswers((prev) => [...prev, answerForm]);
  };

  return (
    <Fragment>
      <h3>question</h3>
      <input type="text" onChange={handlequestion} value={question} />
      <button onClick={addAnswer}>답변 추가</button>
      {answers.map((value, index) => {
        return (
          <div key={index}>
            <h3>answer</h3>
            <input
              type="text"
              onChange={handleAnswer(index)}
              value={value.answer}
            />
            <h3>response</h3>
            <input
              type="text"
              onChange={handleResponse(index)}
              value={value.response}
            />
          </div>
        );
      })}
      <button onClick={handleUpload}>업로드</button>
    </Fragment>
  );
};

export default SurveyAdd;
