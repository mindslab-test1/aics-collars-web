import React, { Fragment } from "react";

// Components
import { StyledLink } from "../../components/utils/Utils";
// Routes
import { DashboardRoute } from "../../routes/Routes";

const Dashboard = () => {
  return (
    <Fragment>
      <h2>Dashboard Home</h2>
      <StyledLink to="/">MAIN</StyledLink>
      {DashboardRoute.map((route, index) => {
        if (route.display) {
          return (
            <div key={route.name}>
              <StyledLink to={route.path}>{route.name}</StyledLink>
            </div>
          );
        } else return null;
      })}
    </Fragment>
  );
};

export default Dashboard;
