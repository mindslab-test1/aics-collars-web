import React, { Fragment, useState } from "react";
import { ExamListDialog } from "../../../components/dialogs/ExamListDIalog";
import {
  upload,
  EFFECT_VOCA,
  EFFECT_GRAMMAR,
  EFFECT_CONTEXT,
  EFFECT_TONE,
  EFFECT_KNOWLEDGE,
  THUMBNAIL,
} from "../../../actions/session";

const SessionAdd = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [title, setTitle] = useState("");
  const [expiration, setExpiration] = useState(0);
  const [introduction, setIntroduction] = useState("");
  const [goal, setGoal] = useState([]);
  const [effect, setEffect] = useState({
    voca: 0,
    grammar: 0,
    context: 0,
    tone: 0,
    knowledge: 0,
  });
  const [images, setImages] = useState({
    THUMBNAIL: null,
  });
  const [exam, setExam] = useState({
    title: "",
    id: null,
  });
  const [video, setVideo] = useState({ file: null, duration: 0, title: "" });

  const handleName = (e) => setName(e.target.value);
  const handlePrice = (e) => setPrice(parseInt(e.target.value));
  const handleTitle = (e) => setTitle(e.target.value);
  const handleExpiration = (e) => setExpiration(parseInt(e.target.value));
  const handleIntroduction = (e) => setIntroduction(e.target.value);
  const handleGoal = (index) => (e) => {
    e.preventDefault();
    const newGoal = [...goal];
    newGoal[index] = e.target.value;
    setGoal(newGoal);
  };
  const handleEffect = (type) => (e) => {
    e.preventDefault();
    const value = parseInt(e.target.value);

    switch (type) {
      case EFFECT_VOCA:
        setEffect((prev) => ({ ...prev, voca: value }));
        break;
      case EFFECT_GRAMMAR:
        setEffect((prev) => ({ ...prev, grammar: value }));
        break;
      case EFFECT_CONTEXT:
        setEffect((prev) => ({ ...prev, context: value }));
        break;
      case EFFECT_TONE:
        setEffect((prev) => ({ ...prev, tone: value }));
        break;
      case EFFECT_KNOWLEDGE:
        setEffect((prev) => ({ ...prev, knowledge: value }));
        break;
      default:
        break;
    }
  };
  const handleVideoTitle = (e) =>
    setVideo((prev) => ({ ...prev, title: e.target.value }));
  const handleVideo = async (e) => {
    setIsLoading(true);
    const file = e.target.files[0];
    await getDuration(file, async (duration) => {
      setVideo((prev) => ({ ...prev, file, duration }));
      setIsLoading(false);
    });
  };
  const handleImages = (type) => (e) => {
    e.preventDefault();
    switch (type) {
      case THUMBNAIL:
        setImages((prev) => ({ ...prev, THUMBNAIL: e.target.files[0] }));
        break;
      default:
        break;
    }
  };
  const handleExamTitle = (e) => {
    setExam((prev) => ({ ...prev, title: e.target.value }));
  };
  const handleExamId = (value) => {
    setExam((prev) => ({ ...prev, id: value["id"] }));
  };
  const handleUpload = async (e) => {
    setIsLoading(true);
    const data = {
      title,
      expiration,
      introduction,
      goal,
      effect,
    };
    await upload({ name, price, data, video, images, exam });
    setIsLoading(false);
  };

  const getDuration = async (file, callback) => {
    let v = document.createElement("video");
    v.src = URL.createObjectURL(file);
    v.onloadedmetadata = () => {
      callback(v.duration);
    };
  };

  const addGoal = (e) => {
    e.preventDefault();
    const newGoal = "";
    setGoal((prev) => [...prev, newGoal]);
  };

  return (
    <Fragment>
      <h3>제목</h3>
      <input type="text" onChange={handleName} value={name} />
      <h4>가격</h4>
      <input type="number" onChange={handlePrice} value={price} min="0" />
      <h4>타이틀</h4>
      <input type="text" onChange={handleTitle} value={title} />
      <h4>기한</h4>
      <input
        type="number"
        onChange={handleExpiration}
        value={expiration}
        min="0"
      />
      <h4>소개</h4>
      <input type="text" onChange={handleIntroduction} value={introduction} />
      <h4>목표</h4>
      <button onClick={addGoal}>추가</button>
      {goal.map((value, index) => {
        return (
          <div key={index}>
            <input
              type="text"
              onChange={handleGoal(index)}
              value={goal[index]}
            />
          </div>
        );
      })}
      <h4>효과</h4>
      <h6>Voca</h6>
      <input
        type="number"
        onChange={handleEffect(EFFECT_VOCA)}
        value={effect["voca"]}
        min="0"
        max="5"
      />
      <h6>grammar</h6>
      <input
        type="number"
        onChange={handleEffect(EFFECT_GRAMMAR)}
        value={effect["grammar"]}
        min="0"
        max="5"
      />
      <h6>context</h6>
      <input
        type="number"
        onChange={handleEffect(EFFECT_CONTEXT)}
        value={effect["context"]}
        min="0"
        max="5"
      />
      <h6>tone</h6>
      <input
        type="number"
        onChange={handleEffect(EFFECT_TONE)}
        value={effect["tone"]}
        min="0"
        max="5"
      />
      <h6>knowledge</h6>
      <input
        type="number"
        onChange={handleEffect(EFFECT_KNOWLEDGE)}
        value={effect["knowledge"]}
        min="0"
        max="5"
      />
      <br />
      <h3>동영상</h3>
      <h4>동영상 타이틀</h4>
      <input type="text" onChange={handleVideoTitle} value={video["title"]} />
      <br />
      <input type="file" accept="video/*" onChange={handleVideo} />
      <h3>Thumbnail</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(THUMBNAIL)}
      />
      <h4>연동 Exam</h4>
      <input type="text" onChange={handleExamTitle} value={exam["title"]} />
      <ExamListDialog onChange={handleExamId} />
      <br />
      {isLoading ? (
        <p>업로드 중</p>
      ) : (
        <button onClick={handleUpload}>업로드</button>
      )}
    </Fragment>
  );
};

export default SessionAdd;
