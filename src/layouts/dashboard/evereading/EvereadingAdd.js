import React, { Fragment, useState } from "react";
import styled from "styled-components";

import {
  upload,
  THUMBNAIL,
  INTRO_BANNER,
  BANNER,
} from "../../../actions/evereading";

const Section = styled.div`
  display: flex;
`;
const SectionItem = styled.div`
  flex: 1;
`;

const EvereadingAdd = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [level, setLevel] = useState(0);
  const [sector, setSector] = useState({ eng: "", kor: "" });
  const [title, setTitle] = useState({ eng: "", kor: "" });
  const [description, setDescription] = useState({ eng: "", kor: "" });
  const [content, setContent] = useState({ eng: [], kor: [] });
  const [vocabulary, setVocabulary] = useState([]);
  const [images, setImages] = useState({
    THUMBNAIL: null,
    INTRO_BANNER: null,
    BANNER: null,
  });

  const handleName = (e) => setName(e.target.value);
  const handleLevel = (e) => setLevel(e.target.value);
  const handleSector = (type) => (e) => {
    e.preventDefault();

    switch (type) {
      case "eng":
        setSector((prev) => ({ ...prev, eng: e.target.value }));
        break;
      case "kor":
        setSector((prev) => ({ ...prev, kor: e.target.value }));
        break;
      default:
        break;
    }
  };
  const handleTitle = (type) => (e) => {
    e.preventDefault();

    switch (type) {
      case "eng":
        setTitle((prev) => ({ ...prev, eng: e.target.value }));
        break;
      case "kor":
        setTitle((prev) => ({ ...prev, kor: e.target.value }));
        break;
      default:
        break;
    }
  };
  const handleDescription = (type) => (e) => {
    e.preventDefault();

    switch (type) {
      case "eng":
        setDescription((prev) => ({ ...prev, eng: e.target.value }));
        break;
      case "kor":
        setDescription((prev) => ({ ...prev, kor: e.target.value }));
        break;
      default:
        break;
    }
  };
  const handleContent = ({ lang, type, index }) => async (e) => {
    e.preventDefault();

    const checkType = (newContent) => {
      switch (type.toUpperCase()) {
        case "HEADER":
          newContent[index].header = e.target.value;
          break;
        case "TEXT":
          newContent[index].text = e.target.value;
          break;
        default:
          break;
      }
      return newContent;
    };

    switch (lang.toUpperCase()) {
      case "ENG": {
        const newContent = await checkType([...content.eng]);
        setContent((prev) => ({ ...prev, eng: newContent }));
        break;
      }
      case "KOR": {
        const newContent = await checkType([...content.kor]);
        setContent((prev) => ({ ...prev, kor: newContent }));
        break;
      }
      default:
        break;
    }
  };
  const handleVocabulary = (index, type) => (e) => {
    e.preventDefault();
    const newVocabulary = [...vocabulary];
    switch (type) {
      case "eng":
        newVocabulary[index].eng = e.target.value;
        break;
      case "kor":
        newVocabulary[index].kor = e.target.value;
        break;
      default:
        break;
    }
    setVocabulary(newVocabulary);
  };
  const handleImages = (type) => (e) => {
    e.preventDefault();
    switch (type) {
      case THUMBNAIL:
        setImages((prev) => ({ ...prev, THUMBNAIL: e.target.files[0] }));
        break;
      case INTRO_BANNER:
        setImages((prev) => ({ ...prev, INTRO_BANNER: e.target.files[0] }));
        break;
      case BANNER:
        setImages((prev) => ({ ...prev, BANNER: e.target.files[0] }));
        break;
      default:
        break;
    }
  };
  const handleUpload = async (e) => {
    setIsLoading(true);
    const data = {
      eng: {
        title: title.eng,
        description: description.eng,
        content: content.eng,
      },
      kor: {
        title: title.kor,
        description: description.kor,
        content: content.kor,
      },
      vocabulary,
    };
    await upload({ name, sector, level, data, images });
    setIsLoading(false);
  };

  const addVocabulary = (e) => {
    e.preventDefault();
    const vocabularyForm = {
      index: undefined,
      eng: "",
      kor: "",
    };
    setVocabulary((prev) => {
      vocabularyForm.index = prev.length + 1;
      return [...prev, vocabularyForm];
    });
  };
  const addContent = (e) => {
    e.preventDefault();
    setContent((prev) => ({
      eng: [
        ...prev.eng,
        {
          header: "",
          text: "",
        },
      ],
      kor: [
        ...prev.kor,
        {
          header: "",
          text: "",
        },
      ],
    }));
  };

  return (
    <Fragment>
      <h1>Evereading Add</h1>
      <h3>제목</h3>
      <input type="text" onChange={handleName} value={name} />
      <h3>Sector</h3>
      <h4>Eng</h4>
      <input type="text" onChange={handleSector("eng")} value={sector.eng} />
      <h4>Kor</h4>
      <input type="text" onChange={handleSector("kor")} value={sector.kor} />
      <h3>Level</h3>
      <select onChange={handleLevel}>
        {(() => {
          let options = [];
          for (let i = 0; i < 11; i++) {
            options.push(
              <option key={i} value={i}>
                {i}
              </option>
            );
          }
          return options;
        })()}
      </select>
      <Section>
        <SectionItem>
          <h3>English</h3>
          <h4>title</h4>
          <input type="text" onChange={handleTitle("eng")} value={title.eng} />
          <h4>description</h4>
          <input
            type="text"
            onChange={handleDescription("eng")}
            value={description.eng}
          />
        </SectionItem>
        <SectionItem>
          <h3>Korean</h3>
          <h4>title</h4>
          <input type="text" onChange={handleTitle("kor")} value={title.kor} />
          <h4>description</h4>
          <input
            type="text"
            onChange={handleDescription("kor")}
            value={description.kor}
          />
        </SectionItem>
      </Section>
      <br />
      <button onClick={addContent}>Content 추가</button>
      <Section>
        <SectionItem>
          {content.eng.map((value, index) => {
            return (
              <div key={index}>
                <h4>HEADER</h4>
                <input
                  type="text"
                  onChange={handleContent({
                    lang: "eng",
                    type: "header",
                    index: index,
                  })}
                  value={value.header}
                />
                <h4>TEXT</h4>
                <input
                  type="text"
                  onChange={handleContent({
                    lang: "eng",
                    type: "text",
                    index: index,
                  })}
                  value={value.text}
                />
              </div>
            );
          })}
        </SectionItem>
        <SectionItem>
          {content.kor.map((value, index) => {
            return (
              <div key={index}>
                <h4>HEADER</h4>
                <input
                  type="text"
                  onChange={handleContent({
                    lang: "kor",
                    type: "header",
                    index: index,
                  })}
                  value={value.header}
                />
                <h4>TEXT</h4>
                <input
                  type="text"
                  onChange={handleContent({
                    lang: "kor",
                    type: "text",
                    index: index,
                  })}
                  value={value.text}
                />
              </div>
            );
          })}
        </SectionItem>
      </Section>

      <h3>단어</h3>
      <button onClick={addVocabulary}>단어 추가</button>
      {vocabulary.map((value, index) => {
        return (
          <div key={index}>
            <span>eng</span>
            <input
              type="text"
              onChange={handleVocabulary(index, "eng")}
              value={value.eng}
            />
            <span>kor</span>
            <input
              type="text"
              onChange={handleVocabulary(index, "kor")}
              value={value.kor}
            />
          </div>
        );
      })}

      <h2>이미지</h2>
      <h3>Thumbnail</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(THUMBNAIL)}
      />
      <h3>IntroBanner</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(INTRO_BANNER)}
      />
      <h3>banner</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(BANNER)}
      />
      {isLoading ? (
        <p>업로드 중</p>
      ) : (
        <button onClick={handleUpload}>업로드</button>
      )}
    </Fragment>
  );
};

export default EvereadingAdd;
