import React, { Fragment, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import PropTypes from "prop-types";

import { loadDataList } from "../../../actions/twoCents";

const TwoCentsList = () => {
  const history = useHistory();
  const [current, setCurrent] = useState(0);
  const [data, setData] = useState([]);

  useEffect(() => {
    setCurrent(data.length);
  }, [data, setCurrent]);

  const handleLoadDataList = async (e) => {
    e.preventDefault();
    const newDataList = await loadDataList({ current });
    setData((prev) => [...prev, ...newDataList]);
  };
  const handleUpdate = (id) => (e) => {
    e.preventDefault();
    // history.push(`/dashboard/twoCents/ExamUpdate/${id}`);
  };

  return (
    <Fragment>
      <button onClick={handleLoadDataList}>데이터 가져오기</button>
      {data.map((value, index) => {
        return (
          <div key={index} onClick={handleUpdate(value.id)}>
            <h3>Name: {value.name}</h3>
            <p>Id: {value.id}</p>
            <p>Level: {value.lev}</p>
            <p>date: {value.createDate}</p>
          </div>
        );
      })}
    </Fragment>
  );
};

export default TwoCentsList;
