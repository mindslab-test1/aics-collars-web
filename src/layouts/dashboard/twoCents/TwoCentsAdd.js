import React, { Fragment, useState } from "react";
import {
  upload,
  THUMBNAIL,
  INTRO_BANNER,
  BANNER,
} from "../../../actions/twoCents";

const TwoCentsAdd = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [level, setLevel] = useState(0);
  const [sector, setSector] = useState({ eng: "", kor: "" });
  const [title, setTitle] = useState({ eng: "", kor: "" });
  const [content, setContent] = useState([]);
  const [vocabulary, setVocabulary] = useState([]);
  const [files, setFiles] = useState([]);
  const [images, setImages] = useState({
    THUMBNAIL: null,
    INTRO_BANNER: null,
    BANNER: null,
  });

  const handleName = (e) => setName(e.target.value);
  const handleTitle = (type) => (e) => {
    e.preventDefault();

    switch (type) {
      case "eng":
        setTitle((prev) => ({ ...prev, eng: e.target.value }));
        break;
      case "kor":
        setTitle((prev) => ({ ...prev, kor: e.target.value }));
        break;
      default:
        break;
    }
  };
  const handleLevel = (e) => setLevel(e.target.value);
  const handleSector = (type) => (e) => {
    e.preventDefault();

    switch (type) {
      case "eng":
        setSector((prev) => ({ ...prev, eng: e.target.value }));
        break;
      case "kor":
        setSector((prev) => ({ ...prev, kor: e.target.value }));
        break;
      default:
        break;
    }
  };
  const handleContent = (index, type) => (e) => {
    e.preventDefault();
    const newContent = [...content];
    switch (type) {
      case "name":
        newContent[index].name = e.target.value;
        break;
      case "eng":
        newContent[index].eng = e.target.value;
        break;
      case "kor":
        newContent[index].kor = e.target.value;
        break;
      default:
        break;
    }
    setContent(newContent);
  };
  const handleContentFloating = (index, float) => (e) => {
    e.preventDefault();
    const newContent = [...content];
    newContent[index].floating = float === "left" ? "right" : "left";
    setContent(newContent);
  };
  const handleContentFile = (index) => (e) => {
    e.preventDefault();
    const newFiles = [...files];
    newFiles[index] = e.target.files[0];
    // newFiles[index] = e.target.files;
    setFiles(newFiles);
  };
  const handleVocabulary = (index, type) => (e) => {
    e.preventDefault();
    const newVocabulary = [...vocabulary];
    switch (type) {
      case "eng":
        newVocabulary[index].eng = e.target.value;
        break;
      case "kor":
        newVocabulary[index].kor = e.target.value;
        break;
      default:
        break;
    }
    setVocabulary(newVocabulary);
  };
  const handleImages = (type) => (e) => {
    e.preventDefault();
    switch (type) {
      case THUMBNAIL:
        setImages((prev) => ({ ...prev, THUMBNAIL: e.target.files[0] }));
        break;
      case INTRO_BANNER:
        setImages((prev) => ({ ...prev, INTRO_BANNER: e.target.files[0] }));
        break;
      case BANNER:
        setImages((prev) => ({ ...prev, BANNER: e.target.files[0] }));
        break;
      default:
        break;
    }
  };
  const handleUpload = async (e) => {
    setIsLoading(true);
    const data = {
      title,
      content,
      vocabulary,
    };

    await upload({ name, sector, level, data, files, images });
    setIsLoading(false);
  };

  const addVocabulary = (e) => {
    e.preventDefault();
    const vocabularyForm = {
      index: undefined,
      eng: "",
      kor: "",
    };
    setVocabulary((prev) => {
      vocabularyForm.index = prev.length + 1;
      return [...prev, vocabularyForm];
    });
  };
  const addContent = (e) => {
    e.preventDefault();
    const contentForm = {
      floating: "left",
      name: "",
      eng: "",
      kor: "",
      audio: "",
    };
    setContent((prev) => [...prev, contentForm]);
  };

  return (
    <Fragment>
      <h1>TwoCents Add</h1>
      <h3>제목</h3>
      <input type="text" onChange={handleName} value={name} />
      <h3>Sector</h3>
      <h4>Eng</h4>
      <input type="text" onChange={handleSector("eng")} value={sector.eng} />
      <h4>Kor</h4>
      <input type="text" onChange={handleSector("kor")} value={sector.kor} />
      <h3>Level</h3>
      <select onChange={handleLevel}>
        {(() => {
          let options = [];
          for (let i = 0; i < 11; i++) {
            options.push(
              <option key={i} value={i}>
                {i}
              </option>
            );
          }
          return options;
        })()}
      </select>
      <h3>영어 타이틀</h3>
      <input type="text" onChange={handleTitle("eng")} value={title.eng} />
      <h3>한글 타이틀</h3>
      <input type="text" onChange={handleTitle("kor")} value={title.kor} />
      <h3>컨텐츠</h3>
      <button onClick={addContent}>컨텐츠 추가</button>
      {content.map((value, index) => {
        return (
          <div key={index}>
            <button onClick={handleContentFloating(index, value.floating)}>
              {value.floating.toUpperCase()}
            </button>
            <br />
            <span>name</span>
            <input
              type="text"
              onChange={handleContent(index, "name")}
              value={value.name}
            />
            <span>eng</span>
            <input
              type="text"
              onChange={handleContent(index, "eng")}
              value={value.eng}
            />
            <span>kor</span>
            <input
              type="text"
              onChange={handleContent(index, "kor")}
              value={value.kor}
            />
            <span>audio</span>
            <input
              type="file"
              accept="audio/*"
              onChange={handleContentFile(index)}
            />
          </div>
        );
      })}
      <h3>단어</h3>
      <button onClick={addVocabulary}>단어 추가</button>
      {vocabulary.map((value, index) => {
        return (
          <div key={index}>
            <span>eng</span>
            <input
              type="text"
              onChange={handleVocabulary(index, "eng")}
              value={value.eng}
            />
            <span>kor</span>
            <input
              type="text"
              onChange={handleVocabulary(index, "kor")}
              value={value.kor}
            />
          </div>
        );
      })}
      <h2>이미지</h2>
      <h3>Thumbnail</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(THUMBNAIL)}
      />
      <h3>IntroBanner</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(INTRO_BANNER)}
      />
      <h3>banner</h3>
      <input
        type="file"
        accept="image/x-png,image/jpeg"
        onChange={handleImages(BANNER)}
      />
      <br />
      <br />
      {isLoading ? (
        <p>업로드 중</p>
      ) : (
        <button onClick={handleUpload}>업로드</button>
      )}
    </Fragment>
  );
};

export default TwoCentsAdd;
