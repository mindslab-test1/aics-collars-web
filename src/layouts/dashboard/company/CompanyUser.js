import React, { Fragment, useState, useEffect } from "react";
import styled from "styled-components";

import { getCompanies, generateUser } from "../../../actions/company";

const Box = styled.div`
  width: 150px;
  border: 2px solid black;
  border-radius: 25px;
  padding: 3px;
  background-color: ${(props) => (props.checked ? "gray" : null)};
`;

const Section = styled.div`
  display: flex;
`;
const SectionItem = styled.div`
  flex: 1;
`;

const CompanyUser = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [numbers, setNumbers] = useState(1);
  const [companies, setCompanies] = useState([]);
  const [company, setCompany] = useState();
  const [newUsers, setNewUsers] = useState([]);

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    let data = await getCompanies();
    setCompanies(data);
  };
  const handleNumber = (e) => setNumbers(e.target.value);
  const handleClick = (value) => (e) => {
    e.preventDefault();
    setCompany(value);
  };
  const handleSubmit = async () => {
    setIsLoading(true);
    const result = await generateUser({ companyId: company, numbers });
    setNewUsers(result);
    setIsLoading(false);
  };

  return (
    <Fragment>
      <div>
        <h4>추가할 인원 수</h4>
        <input type="number" min="1" value={numbers} onChange={handleNumber} />
        {isLoading ? (
          <p>로딩중</p>
        ) : (
          <button onClick={handleSubmit}>보내기</button>
        )}
        <br />
        <br />
        <Section>
          <SectionItem>
            {companies.map((value, index) => {
              return (
                <Box
                  key={index}
                  onClick={handleClick(value.id)}
                  checked={value.id === company ? true : false}
                >
                  <h4>회사명: {value.name}</h4>
                  <h5>코드: {value.code}</h5>
                </Box>
              );
            })}
          </SectionItem>
          <SectionItem>
            <table align="left">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>PW</th>
                </tr>
              </thead>
              <tbody>
                {newUsers.map((value, index) => {
                  return (
                    <tr key={index}>
                      <td>{value.id}</td>
                      <td>{value.password}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </SectionItem>
        </Section>
      </div>
    </Fragment>
  );
};

export default CompanyUser;
