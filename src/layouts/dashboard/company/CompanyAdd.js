import React, { Fragment, useState } from "react";

import { addCategory } from "../../../actions/company";

const CompanyAdd = () => {
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState("");
  const [code, setCode] = useState("");

  const handleName = (e) => setName(e.target.value);
  const handleCode = (e) => setCode(e.target.value);
  const handleSubmit = async () => {
    setIsLoading(true);
    await addCategory({ name, code });
    setIsLoading(false);
  };

  return (
    <Fragment>
      <h1>회사 추가</h1>
      <h3>회사 이름</h3>
      <input type="text" onChange={handleName} value={name} />
      <br />
      <h3>회사 코드</h3>
      <input type="text" onChange={handleCode} value={code} />
      <br /> <br />
      {isLoading ? (
        <p>로딩중</p>
      ) : (
        <button onClick={handleSubmit}>보내기</button>
      )}
    </Fragment>
  );
};

export default CompanyAdd;
