import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { updateData } from "../../actions/exam";

const Highlight = ({ data, index, updateData }) => {
  const initData = data[index];
  const [question, setQuestion] = useState(initData ? initData.question : "");
  const [subQuestion, setSubQuestion] = useState(
    initData ? initData.subQuestion : []
  );
  const [answer, setAnswer] = useState(initData ? initData.answer : []);

  useEffect(() => {
    const highlight = {
      type: "HIGHLIGHT",
      question: question,
      subQuestion: subQuestion,
      answer: answer,
    };

    updateData({ data: highlight, index: index });
  }, [index, question, subQuestion, answer, updateData]);
  // Handler
  const handleQuestion = (e) => setQuestion(e.target.value);
  const handleSubQuestionSpeaker = (index) => (e) => {
    const newSubQuestion = [...subQuestion];
    newSubQuestion[index].speaker = e.target.value;
    setSubQuestion(newSubQuestion);
  };
  const handleSubQuestionText = (index) => (e) => {
    const { value } = e.target;

    const newSubQuestion = [...subQuestion];
    newSubQuestion[index].text = value;
    setSubQuestion(newSubQuestion);
    addAnswer(index, value);
  };
  const handleAnswer = (sentenceIdx, wordIdx) => (e) => {
    e.preventDefault();
    const newAnswer = [...answer];
    newAnswer[sentenceIdx][wordIdx].is_answer = !newAnswer[sentenceIdx][wordIdx]
      .is_answer;
    setAnswer(newAnswer);
  };
  const addSubQuestion = (e) => {
    e.preventDefault();
    setSubQuestion([...subQuestion, { speaker: "", text: "" }]);
  };
  const addAnswer = async (index, text) => {
    const textList = text.split(/\b/).filter((word) => word !== "");
    let newText = [];
    textList.forEach((value) => {
      newText.push({ word: value, is_answer: false });
    });

    const newAnswer = [...answer];
    newAnswer[index] = newText;
    setAnswer(newAnswer);
  };

  return (
    <Fragment>
      <h2>HIGHLIGHT</h2>
      <h3>문제</h3>
      <input type="text" onChange={handleQuestion} value={question} />
      <button onClick={addSubQuestion}>지문 Add</button>
      {subQuestion.map((value, index) => {
        return (
          <div key={index}>
            <span>SPEAKER</span>
            <input
              type="text"
              onChange={handleSubQuestionSpeaker(index)}
              value={value.speaker}
            />
            <span>TEXT</span>
            <input
              type="text"
              onChange={handleSubQuestionText(index)}
              value={value.text}
            />
          </div>
        );
      })}
      <h3>정답 선택</h3>
      {answer.map((sentence, sentenceIdx) => {
        return (
          <div key={sentenceIdx}>
            {sentence.map((value, index) => {
              return (
                <span
                  key={index}
                  onClick={
                    value.word.indexOf("") !== -1
                      ? handleAnswer(sentenceIdx, index)
                      : null
                  }
                  style={{ color: value.is_answer ? "blue" : "black" }}
                >
                  {value.word}
                </span>
              );
            })}
          </div>
        );
      })}
    </Fragment>
  );
};

Highlight.propTypes = {
  data: PropTypes.array,
  index: PropTypes.number,
  updateData: PropTypes.func,
};
const mapStateToProps = (state) => ({
  data: state.exam.data,
});

export default connect(mapStateToProps, { updateData })(Highlight);
