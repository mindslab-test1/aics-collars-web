import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { updateData } from "../../actions/exam";

const ShortAnswer = ({ data, index, updateData }) => {
  const initData = data[index];
  const [question, setQuestion] = useState(initData ? initData.question : "");
  const [subQuestion, setSubQuestion] = useState(
    initData ? initData.subQuestion[0] : ""
  );
  const [answer, setAnswer] = useState(initData ? initData.answer : []);

  useEffect(() => {
    const shortAnswer = {
      type: "SHORT_ANSWER",
      question: question,
      subQuestion: [subQuestion],
      answer: answer,
    };

    updateData({ data: shortAnswer, index: index });
  }, [index, question, subQuestion, answer, updateData]);
  // Handler
  const handleQuestion = (e) => setQuestion(e.target.value);
  const handleSubQuestion = (e) => setSubQuestion(e.target.value);
  const handleAnswer = (index) => (e) => {
    const newAnswer = [...answer];
    newAnswer[index] = e.target.value;
    setAnswer(newAnswer);
  };
  const addAnswer = (e) => {
    e.preventDefault();
    setAnswer([...answer, ""]);
  };

  return (
    <Fragment>
      <h2>ShortAnswer</h2>
      <button onClick={addAnswer}>ADD</button>
      <h3>한글 질문</h3>
      <input type="text" onChange={handleQuestion} value={question} />
      <h3>영어 질문</h3>
      <input type="text" onChange={handleSubQuestion} value={subQuestion} />
      {answer.map((value, index) => {
        return (
          <div key={index}>
            <h3>답변 {index + 1}</h3>
            <input type="text" onChange={handleAnswer(index)} value={value} />
          </div>
        );
      })}
    </Fragment>
  );
};

ShortAnswer.propTypes = {
  data: PropTypes.array,
  index: PropTypes.number,
  updateData: PropTypes.func,
};
const mapStateToProps = (state) => ({
  data: state.exam.data,
});

export default connect(mapStateToProps, { updateData })(ShortAnswer);
