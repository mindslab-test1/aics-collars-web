import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { updateData } from "../../actions/exam";

const Speaking = ({ data, index, updateData }) => {
  const initData = data[index];
  const [question, setQuestion] = useState(initData ? initData.question : "");
  const [subQuestion, setSubQuestion] = useState(
    initData ? initData.subQuestion : []
  );
  const [answer, setAnswer] = useState(initData ? initData.answer : []);

  useEffect(() => {
    const speaking = {
      type: "SPEAKING",
      question: question,
      subQuestion: subQuestion,
      answer: answer,
    };

    updateData({ data: speaking, index: index });
  }, [index, question, subQuestion, answer, updateData]);
  // Handler
  const handleQuestion = (e) => setQuestion(e.target.value);
  const handleSubQuestionSpeaker = (index) => (e) => {
    const newSubQuestion = [...subQuestion];
    newSubQuestion[index].speaker = e.target.value;
    setSubQuestion(newSubQuestion);
  };
  const handleSubQuestionText = (index) => (e) => {
    const newSubQuestion = [...subQuestion];
    newSubQuestion[index].text = e.target.value;
    setSubQuestion(newSubQuestion);
  };
  const handleAnswer = (index) => (e) => {
    const newAnswer = [...answer];
    newAnswer[index] = e.target.value;
    setAnswer(newAnswer);
  };
  const addSubQuestion = (e) => {
    e.preventDefault();
    setSubQuestion([...subQuestion, { speaker: "", text: "" }]);
  };
  const addAnswer = (e) => {
    e.preventDefault();
    setAnswer([...answer, ""]);
  };

  return (
    <Fragment>
      <h2>SPEAKING</h2>
      <h3>문제</h3>
      <input type="text" onChange={handleQuestion} value={question} />
      <h3>지문</h3>
      <button onClick={addSubQuestion}>지문 Add</button>
      {subQuestion.map((value, index) => {
        return (
          <div key={index}>
            <span>SPEAKER</span>
            <input
              type="text"
              onChange={handleSubQuestionSpeaker(index)}
              value={value.speaker}
            />
            <span>TEXT</span>
            <input
              type="text"
              onChange={handleSubQuestionText(index)}
              value={value.text}
            />
          </div>
        );
      })}
      <button onClick={addAnswer}>답변 Add</button>
      {answer.map((value, index) => {
        return (
          <div key={index}>
            <input type="text" onChange={handleAnswer(index)} value={value} />
          </div>
        );
      })}
    </Fragment>
  );
};

Speaking.propTypes = {
  data: PropTypes.array,
  index: PropTypes.number,
  updateData: PropTypes.func,
};
const mapStateToProps = (state) => ({
  data: state.exam.data,
});

export default connect(mapStateToProps, { updateData })(Speaking);
