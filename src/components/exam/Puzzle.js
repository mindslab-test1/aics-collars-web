import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { updateData } from "../../actions/exam";

const Puzzle = ({ data, index, updateData }) => {
  const initData = data[index];
  const [question, setQuestion] = useState(initData ? initData.question : "");
  const [answer, setAnswer] = useState(initData ? initData.answer : []);

  useEffect(() => {
    const puzzle = {
      type: "PUZZLE",
      question: question,
      subQuestion: null,
      answer: answer,
    };

    updateData({ data: puzzle, index: index });
  }, [index, question, answer, updateData]);
  // Handler
  const handleQuestion = (e) => setQuestion(e.target.value);
  const handleAnswer = (index) => (e) => {
    const newAnswer = [...answer];
    newAnswer[index] = e.target.value;
    setAnswer(newAnswer);
  };
  const addAnswer = (e) => {
    e.preventDefault();
    setAnswer([...answer, ""]);
  };

  return (
    <Fragment>
      <h2>PUZZLE</h2>
      <button onClick={addAnswer}>ADD</button>
      <form>
        <h3>한글 질문</h3>
        <input type="text" onChange={handleQuestion} value={question} />
        <h3>답변</h3>
        {answer.map((value, index) => {
          return (
            <div key={index}>
              <input type="text" onChange={handleAnswer(index)} value={value} />
            </div>
          );
        })}
      </form>
    </Fragment>
  );
};

Puzzle.propTypes = {
  data: PropTypes.array,
  index: PropTypes.number,
  updateData: PropTypes.func,
};
const mapStateToProps = (state) => ({
  data: state.exam.data,
});

export default connect(mapStateToProps, { updateData })(Puzzle);
