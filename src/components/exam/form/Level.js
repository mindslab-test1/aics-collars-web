import React, { Fragment } from "react";
import { Choice, Highlight, Puzzle, ShortAnswer, Speaking } from "../Exam";

const Level = () => {
  return (
    <Fragment>
      <Choice index={0} />
      <Puzzle index={1} />
      <Speaking index={2} />
      <ShortAnswer index={3} />
      <Highlight index={4} />
    </Fragment>
  );
};
export default Level;
