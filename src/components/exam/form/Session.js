import React, { Fragment } from "react";
import { Choice, Puzzle, ShortAnswer } from "../Exam";

const Session = () => {
  return (
    <Fragment>
      <Choice index={0} />
      <ShortAnswer index={1} />
      <Puzzle index={2} />
      <ShortAnswer index={3} />
    </Fragment>
  );
};

export default Session;
