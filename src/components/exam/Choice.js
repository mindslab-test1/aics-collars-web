import React, { Fragment, useState, useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

// Action
import { updateData } from "../../actions/exam";

const Choice = ({ data, index, updateData }) => {
  const limit = 4;
  const initData = data[index];
  const [question, setQuestion] = useState(initData ? initData.question : "");
  const [subQuestion, setSubQuestion] = useState(
    initData ? initData.subQuestion : []
  );
  const [answer, setAnswer] = useState(initData ? initData.answer : []);

  useEffect(() => {
    const choice = {
      type: "CHOICE",
      question: question,
      subQuestion: subQuestion,
      answer: answer,
    };

    updateData({ data: choice, index: index });
  }, [index, question, subQuestion, answer, updateData]);
  // Handler
  const handleQuestion = (e) => {
    setQuestion(e.target.value);
  };
  const handleSubQuestion = (index) => (e) => {
    const newSubQuestion = [...subQuestion];
    newSubQuestion[index] = e.target.value;
    setSubQuestion(newSubQuestion);
  };
  const handleAnswer = (e) => {
    const { checked, value } = e.target;
    const intValue = parseInt(value);

    checked
      ? setAnswer([...answer, intValue])
      : setAnswer(answer.filter((v) => v !== intValue));
  };
  const addSubQuestion = (e) => {
    e.preventDefault();
    if (subQuestion.length < limit) setSubQuestion([...subQuestion, ""]);
  };

  return (
    <Fragment>
      <h2>CHOICE</h2>
      <form>
        <h3>질문</h3>
        <input type="text" onChange={handleQuestion} value={question} />
        <button onClick={addSubQuestion}>ADD</button>
        {subQuestion.map((value, index) => {
          return (
            <div key={index}>
              <h3>답변 {index + 1}</h3>
              <input
                type="checkbox"
                onChange={handleAnswer}
                value={index}
                checked={answer.includes(index)}
              />
              <input
                type="text"
                onChange={handleSubQuestion(index)}
                value={value}
              />
            </div>
          );
        })}
      </form>
    </Fragment>
  );
};

Choice.propTypes = {
  data: PropTypes.array,
  reset: PropTypes.bool,
  index: PropTypes.number,
  updateData: PropTypes.func,
};
const mapStateToProps = (state) => ({
  data: state.exam.data,
});

export default connect(mapStateToProps, { updateData })(Choice);
