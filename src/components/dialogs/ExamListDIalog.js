import React, { Fragment, useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";

import { loadDataList } from "../../actions/exam";

export const ExamListDialog = ({ onChange }) => {
  const [open, setOpen] = React.useState(false);
  const [current, setCurrent] = useState(0);
  const [data, setData] = useState([]);

  useEffect(() => {
    setCurrent(data.length);
  }, [data, setCurrent]);

  const handleOpen = async () => {
    setOpen(true);
    if (current == 0) {
      const newDataList = await loadDataList({ type: "SESSION", current });
      setData((prev) => [...prev, ...newDataList]);
    }
  };
  const handleClose = () => setOpen(false);
  const handleItem = (value) => (e) => {
    e.preventDefault();
    onChange(value);
  };
  const handleLoadItem = async (e) => {
    e.preventDefault();
    const newDataList = await loadDataList({ type: "SESSION", current });
    setData((prev) => [...prev, ...newDataList]);
  };

  return (
    <div>
      <Button onClick={handleOpen}>OPEN</Button>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Exam List</DialogTitle>
        <div style={{ overflowY: "scroll" }}>
          {data.map((value, index) => {
            return (
              <div key={index}>
                <span>NAME: {value.name}</span>
                <Button onClick={handleItem(value)}>선택</Button>
              </div>
            );
          })}
        </div>
        <button onClick={handleLoadItem}>load</button>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            완료
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
