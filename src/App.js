import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// Redux
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./store";
// Routes
import { RouteBuilder, MainRoute, DashboardRoute } from "./routes/Routes";

const App = () => {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router>
          <Switch>
            <Route path="/dashboard">
              <h1>DASHBOARD</h1>
              <Switch>{RouteBuilder({ routes: DashboardRoute })}</Switch>
            </Route>
            <Route>
              <h1>MAIN</h1>
              <Switch>{RouteBuilder({ routes: MainRoute })}</Switch>
            </Route>
          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  );
};

export default App;
