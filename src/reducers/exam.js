import {
  EXAM_TYPES,
  EXAM_TYPE_CHANGE_SUCCESS,
  EXAM_LAOD_DATA,
  EXAM_UPDATE_DATA,
  EXAM_UPLOAD_DATA_SUCCESS,
  EXAM_UPLOAD_DATA_FAIL,
  EXAM_CLEAR,
} from "../actions/exam";

const initialState = {
  types: EXAM_TYPES,
  currentType: EXAM_TYPES[0],
  data: [],
};

const exam = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case EXAM_LAOD_DATA:
      return {
        ...state,
        currentType: payload.currentType,
        data: payload.data,
      };
    case EXAM_TYPE_CHANGE_SUCCESS:
      return {
        ...state,
        currentType: payload.currentType,
        data: [],
      };
    case EXAM_UPDATE_DATA:
      let newData = [...state.data];
      newData[payload.index] = payload.data;

      return {
        ...state,
        data: newData,
      };
    case EXAM_UPLOAD_DATA_SUCCESS:
      return {
        ...state,
        data: [],
      };
    case EXAM_CLEAR:
      console.log("clear");
      return initialState;
    case EXAM_UPLOAD_DATA_FAIL:
    default:
      return state;
  }
};

export default exam;
