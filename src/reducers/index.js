import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import exam from "./exam";

const rootPersistConfig = {
  key: "root",
  storage,
  whitelist: [],
};

const rootReducer = combineReducers({
  exam,
});

export default persistReducer(rootPersistConfig, rootReducer);
