import { RouteItem } from "./Route";

// Layout
import Dashboard from "../layouts/dashboard/Dashboard";
import ExamAdd from "../layouts/dashboard/exam/ExamAdd";
import ExamList from "../layouts/dashboard/exam/ExamList";
import ExamUpdate from "../layouts/dashboard/exam/ExamUpdate";
import SurveyAdd from "../layouts/dashboard/survey/SurveyAdd";
import TwoCentsAdd from "../layouts/dashboard/twoCents/TwoCentsAdd";
import TwoCentsList from "../layouts/dashboard/twoCents/TwoCentsList";
import TwoCentsUpdate from "../layouts/dashboard/twoCents/TwoCentsUpdate";
import EvereadingAdd from "../layouts/dashboard/evereading/EvereadingAdd";
import SessionAdd from "../layouts/dashboard/session/SessionAdd";
import CompanyAdd from "../layouts/dashboard/company/CompanyAdd";
import CompanyUser from "../layouts/dashboard/company/CompanyUser";

export const DashboardRoute = [
  RouteItem({
    path: "/dashboard/company/category",
    name: "Company 추가",
    component: CompanyAdd,
  }),
  RouteItem({
    path: "/dashboard/company/user",
    name: "Company 유저 추가",
    component: CompanyUser,
  }),
  RouteItem({
    path: "/dashboard/session/add",
    name: "Session 추가",
    component: SessionAdd,
  }),
  RouteItem({
    path: "/dashboard/evereading/add",
    name: "EvereadingAdd 추가",
    component: EvereadingAdd,
  }),
  RouteItem({
    path: "/dashboard/twoCents/update/:id",
    name: "TwoCents 업데이트",
    component: TwoCentsUpdate,
    display: false,
  }),
  RouteItem({
    path: "/dashboard/twoCents/list",
    name: "TwoCents 리스트",
    component: TwoCentsList,
  }),
  RouteItem({
    path: "/dashboard/twoCents/add",
    name: "TwoCents 추가",
    component: TwoCentsAdd,
  }),
  RouteItem({
    path: "/dashboard/survey/add",
    name: "설문조사 추가",
    component: SurveyAdd,
  }),
  RouteItem({
    path: "/dashboard/exam/ExamUpdate/:id",
    name: "ExamUpdate",
    component: ExamUpdate,
    display: false,
  }),
  RouteItem({
    path: "/dashboard/exam/list",
    name: "examList",
    component: ExamList,
  }),
  RouteItem({
    path: "/dashboard/exam/add",
    name: "examAdd",
    component: ExamAdd,
  }),
  RouteItem({
    path: "/dashboard",
    name: "dashboard",
    exact: false,
    component: Dashboard,
  }),
];
