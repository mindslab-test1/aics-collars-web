import { RouteItem } from "./Route";

// Layout
import Main from "../layouts/main/Main";

export const MainRoute = [
  RouteItem({
    path: "/",
    name: "main",
    exact: false,
    component: Main,
  }),
];
