import React from "react";
import { Route } from "react-router-dom";

export const RouteItem = ({
  path,
  name,
  exact = true,
  component,
  display = true,
}) => ({
  path,
  name,
  exact,
  component,
  display,
});

export const RouteBuilder = ({ routes, path }) => {
  return routes.map((route, index) => {
    let routePath = path == null ? route.path : `${path}${route.path}`;
    if (route.exact) {
      return (
        <Route
          key={route.name}
          path={routePath}
          exact
          component={route.component}
        />
      );
    } else {
      return (
        <Route key={route.name} path={routePath} component={route.component} />
      );
    }
  });
};
