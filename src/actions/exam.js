import axios from "axios";

export const EXAM_TYPES = ["LEVEL", "SESSION"];
export const EXAM_TYPE_CHANGE_SUCCESS = "EXAM_TYPE_CHANGE_SUCCESS";
export const EXAM_LAOD_DATA = "EXAM_LOAD_DATA";
export const EXAM_UPDATE_DATA = "EXAM_UPDATE_DATA";
export const EXAM_UPLOAD_DATA_SUCCESS = "EXAM_UPLOAD_DATA_SUCCESS";
export const EXAM_UPLOAD_DATA_FAIL = "EXAM_UPLOAD_DATA_FAIL";
export const EXAM_CLEAR = "EXAM_CLEAR";

export const loadDataList = async ({ type = "*", current }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({
    type: type,
    current: current,
    increment: 10,
    data: false,
  });
  const res = await axios.post("/exam/data:list", body, config);
  return res.data.payload;
};

export const updateExamData = () => {};

export const loadData = ({ id }) => async (dispatch) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({ id });
  const res = await axios.post("/exam/data", body, config);
  const { data, type } = res.data.payload;

  dispatch({
    type: EXAM_LAOD_DATA,
    payload: { currentType: type, data },
  });
};

export const changeType = ({ type }) => async (dispatch) => {
  dispatch({
    type: EXAM_TYPE_CHANGE_SUCCESS,
    payload: { currentType: type },
  });
};

export const updateData = ({ data, index }) => (dispatch) => {
  dispatch({
    type: EXAM_UPDATE_DATA,
    payload: { data, index },
  });
};

export const uploadData = ({ name, type, data, duration }) => async (
  dispatch
) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({
    name,
    exam: {
      type,
      data,
    },
    duration,
  });
  try {
    await axios.post("/exam/data:upload", body, config);
    dispatch({
      type: EXAM_UPLOAD_DATA_SUCCESS,
    });
    return true;
  } catch (err) {
    console.error(err);
    dispatch({
      type: EXAM_UPLOAD_DATA_FAIL,
    });
    return false;
  }
};

export const clearExamData = () => (dispatch) => {
  dispatch({
    type: EXAM_CLEAR,
  });
};
