import axios from "axios";

export const EFFECT_VOCA = "EFFECT_VOCA";
export const EFFECT_GRAMMAR = "EFFECT_GRAMMAR";
export const EFFECT_CONTEXT = "EFFECT_CONTEXT";
export const EFFECT_TONE = "EFFECT_TONE";
export const EFFECT_KNOWLEDGE = "EFFECT_KNOWLEDGE";
export const THUMBNAIL = "THUMBNAIL";

export const upload = async ({ name, price, data, video, images, exam }) => {
  try {
    const dataRes = await uploadData({ name, price, data, exam });
    const { payload } = dataRes.data;
    await uploadVideo({ id: payload.id, video });

    if (images[THUMBNAIL] != null) {
      await uploadImage({
        id: payload.id,
        type: THUMBNAIL,
        file: images[THUMBNAIL],
      });
    }
  } catch (err) {
    console.log(err);
  }
};

const uploadData = async ({ name, price, data, exam }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({ name, price, data, exam });
  const res = await axios.post("/session/data:upload", body, config);
  return res;
};
const uploadVideo = async ({ id, video }) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  const body = new FormData();
  body.append("id", id);
  body.append("title", video["title"]);
  body.append("duration", video["duration"]);
  body.append("file", video["file"]);
  const res = await axios.post("/session/video:update", body, config);
  return res;
};

const uploadImage = async ({ id, type, file }) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  const body = new FormData();
  body.append("id", id);
  body.append("type", type);
  body.append("file", file);
  const res = await axios.post("/session/image:update", body, config);
  return res;
};
