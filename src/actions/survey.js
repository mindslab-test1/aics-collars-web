import axios from "axios";

export const upload = async ({ question, answers }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({ question, answers });
  try {
    const res = await axios.post("/survey/data:upload", body, config);
    console.log(res);
    return true;
  } catch (err) {
    return false;
  }
};
