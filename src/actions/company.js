import axios from "axios";

export const addCategory = async ({ name, code }) => {
  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const body = JSON.stringify({ name, code });
    const res = await axios.post("/company/category", body, config);
    return res;
  } catch (e) {
    console.error(e);
  }
};

export const getCompanies = async () => {
  try {
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    const res = await axios.get("/company/companies");
    return res.data.payload;
  } catch (e) {
    console.error(e);
  }
};

export const generateUser = async ({ companyId, numbers }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({ companyId, numbers });
  try {
    const res = await axios.post("/company/user:auto", body, config);
    return res.data.payload;
  } catch (e) {
    console.error(e);
  }
};
