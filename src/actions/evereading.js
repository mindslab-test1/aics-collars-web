import axios from "axios";

export const THUMBNAIL = "THUMBNAIL";
export const INTRO_BANNER = "INTRO_BANNER";
export const BANNER = "BANNER";

export const upload = async ({ name, sector, level, data, images }) => {
  try {
    const dataRes = await uploadData({ name, sector, level, data });
    const { payload } = dataRes.data;
    if (images[THUMBNAIL] != null) {
      await uploadImage({
        id: payload.id,
        type: THUMBNAIL,
        file: images[THUMBNAIL],
      });
    }
    if (images[INTRO_BANNER] != null) {
      await uploadImage({
        id: payload.id,
        type: INTRO_BANNER,
        file: images[INTRO_BANNER],
      });
    }
    if (images[BANNER] != null) {
      await uploadImage({ id: payload.id, type: BANNER, file: images[BANNER] });
    }
  } catch (err) {
    console.log(err);
  }
};
const uploadData = async ({ name, sector, level, data }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({ name, sector, level, data });
  const res = await axios.post("/evereading/data:upload", body, config);
  return res;
};
const uploadImage = async ({ id, type, file }) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  const body = new FormData();
  body.append("id", id);
  body.append("type", type);
  body.append("file", file);
  const res = await axios.post("/evereading/image:update", body, config);
  return res;
};
