import axios from "axios";

export const THUMBNAIL = "THUMBNAIL";
export const INTRO_BANNER = "INTRO_BANNER";
export const BANNER = "BANNER";

// CREATE
export const upload = async ({ name, sector, level, data, files, images }) => {
  try {
    const dataRes = await uploadData({ name, sector, level, data });
    const { payload } = dataRes.data;

    if (files.length > 0) {
      await uploadAudio({ id: payload.id, files: files });
    }

    if (images[THUMBNAIL] != null) {
      await uploadImage({
        id: payload.id,
        type: THUMBNAIL,
        file: images[THUMBNAIL],
      });
    }
    if (images[INTRO_BANNER] != null) {
      await uploadImage({
        id: payload.id,
        type: INTRO_BANNER,
        file: images[INTRO_BANNER],
      });
    }
    if (images[BANNER] != null) {
      await uploadImage({ id: payload.id, type: BANNER, file: images[BANNER] });
    }
  } catch (err) {
    console.log(err);
  }
};
const uploadData = async ({ name, sector, level, data }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({ name, sector, level, data });
  const res = await axios.post("/twoCents/data:upload", body, config);
  return res;
};
const uploadAudio = async ({ id, files }) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  const body = new FormData();
  body.append("id", id);
  files.forEach((file, index) => {
    body.append("index", index);
    body.append("file", file);
  });
  const res = await axios.post("/twoCents/audio:upload", body, config);
  return res;
};
const uploadImage = async ({ id, type, file }) => {
  const config = {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  };
  const body = new FormData();
  body.append("id", id);
  body.append("type", type);
  body.append("file", file);
  const res = await axios.post("/twoCents/image:update", body, config);
  return res;
};

// READ
export const loadDataList = async ({ current }) => {
  const config = {
    headers: {
      "Content-Type": "application/json",
    },
  };
  const body = JSON.stringify({
    type: "*",
    current: current,
    increment: 10,
    data: false,
  });
  const res = await axios.post("/twoCents/data:list", body, config);
  return res.data.payload;
};
